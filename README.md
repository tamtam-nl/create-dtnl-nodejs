# `@dept/create-dtnl-nodejs`

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/) [![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

The CLI tool for initialising a new Node.js project from the `dtnl-nodejs-starter` template.

⚠️ **NOTE:** This project is currently a _proof of concept_.

## Development

Version management configuration for Node.js is provided for [`volta`](https://volta.sh/). We recommend you have this installed to automatically switch between Node.js versions when you enter one of our project directories. This allows for more deterministic and reproducible builds, which makes debugging easier.

### Installation

To install the dependencies:

```bash
npm install
```

You can then run the following to start the project in _dev mode_.

```bash
npm run dev
```

This runs the `build:watch` and `start:debug` commands at the same time.

#### TypeScript Compilation

You can also run the following to only start the compiler in _watch_ mode.

```bash
npm run build:watch
```

You can also build the library without watching the directory:

```bash
npm run build
```

### Testing

The tests for this library use [Jest](https://jestjs.io) as the test runner. Once you've installed the dependencies, you can run the following command in the root of this repository to run the tests:

```bash
npm run test
```

### Versioning

This repo uses [Semantic Versioning](https://semver.org/) (often referred to as _semver_).

## Contributing

Thinking of contributing to this repo? Awesome! 🚀

Please follow our [Contribution guide](CONTRIBUTING.md) and follow the DEPT [Code of Conduct](https://sites.google.com/deptagency.com/employeehandbooknl/home/house-rules/office-related/code-of-conduct).

## Support

Feeling lost? Come and find us in the [`#global-nodejs`](https://deptagency.slack.com/archives/CG2UXMVB4) channel on Slack for some friendly advice!
