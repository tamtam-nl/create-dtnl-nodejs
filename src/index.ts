import path from 'path';
import meow, { Options, AnyFlags } from 'meow';
import prompts, { PromptObject } from 'prompts';
import chalk from 'chalk';
import shell from 'shelljs';

import { LOGO } from './logo.js';
import { TEMPLATE_REPOS, HELP_TEXT } from './config.js';

async function run() {
  const options: Options<AnyFlags> = {
    importMeta: import.meta,
    flags: {
      template: {
        alias: 't',
      },
    },
  };

  const questions: PromptObject[] = [
    {
      type: 'text',
      name: 'name',
      message: 'What should we call your new project?',
      initial: 'my-dept-project',
    },
  ];

  const cli = meow(HELP_TEXT, options);

  console.log(LOGO);

  console.log(
    "🚀 Alright! Let's create a new project!",
    chalk.bold('A new project will be saved into the current directory.'),
  );

  if (!shell.which('git')) {
    console.error('This script requires git. Install git and try again!');
    shell.exit(1);
  }

  const { name } = await prompts(questions);
  const resolvedPath = path.resolve(process.cwd(), name);

  if (
    shell.exec(`git clone ${TEMPLATE_REPOS.default} ${resolvedPath}`).code !== 0
  ) {
    console.error('Cloning from git failed. Is your SSH key set up correctly?');
    shell.exit(1);
  }

  // @TODO: remove .git from project dir and re-init
  // @TODO: rewrite package.json with project data

  // outro block
  console.log();
  console.log(`${chalk.green('Success!')} Created ${name} at ${resolvedPath}`);
  console.log('Inside that directory, you can run several commands:');
  console.log();
  console.log(chalk.cyan(`  npm run dev`));
  console.log('    Starts the development server.');
  console.log();
  console.log(chalk.cyan(`  npm run build`));
  console.log('    Builds the app for production.');
  console.log();
  console.log(chalk.cyan(`  npm run start`));
  console.log('    Runs the built app in production mode.');
  console.log();
  console.log('We suggest that you begin by typing:');
  console.log();
  console.log(chalk.cyan('  cd'), resolvedPath);
  console.log(`  ${chalk.cyan('npm run dev')}`);
}

run();
