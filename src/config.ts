/**
 * Add a template name and Git SSH URL to this object to make
 * it usable as a template
 */
export const TEMPLATE_REPOS = {
  default: 'git@bitbucket.org:tamtam-nl/dtnl-nodejs-starter.git',
};

export const HELP_TEXT = `
Usage
$ npx @dept/dtnl-nodejs

Options
--template, -t  Use a custom template
`;
